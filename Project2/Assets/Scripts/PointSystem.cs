﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointSystem : MonoBehaviour {

	public static PointSystem instance = null;
	public bool shouldUse;
	public Text pointsText;
	public int killPts;
	public int currentPoints;
	public int pointsToWin;

	void Awake() {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (this.gameObject);
		}
		//DontDestroyOnLoad (this.gameObject);
	}
	// Use this for initialization
	void Start () {
		currentPoints = 0;
		UpdateScoreText ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AddPoints() {
		currentPoints += killPts;
		UpdateScoreText ();
	}

	public void UpdateScoreText() {
		pointsText.text = "Points: " + currentPoints;
	}
}
