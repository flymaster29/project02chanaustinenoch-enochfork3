﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDroppedObject : MonoBehaviour {

	public static SpawnDroppedObject instance = null;

	void Awake() {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (this.gameObject);
		}
		//DontDestroyOnLoad (this.gameObject);
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SpawnObject(GameObject item) {
		item.transform.position = new Vector3 (transform.position.x + 3, transform.position.y, transform.position.z + 3);
		item.SetActive (true);
	}
}
