﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	public Transform[] spawnPoint;
	public GameObject enemy;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("SpawnEnemy", 2f, 5f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void SpawnEnemy() {
		int temp = Random.Range (0, 3);

		Instantiate (enemy, spawnPoint [temp].transform.position, Quaternion.identity);
	}

}
